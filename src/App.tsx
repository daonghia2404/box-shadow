// @ts-ignore
// @ts-nocheck
import React, { useState, useEffect } from "react";
import "./app.css";
import "bootstrap/dist/css/bootstrap.min.css";

const App = () => {
  const [shadow, setShadow] = useState([]);

  const [shiftRight, setShiftRight] = useState(0);
  const [shiftDown, setShiftDown] = useState(0);
  const [blur, setBlur] = useState(0);
  const [spread, setSpread] = useState(0);
  const [indexChange, setIndexChange] = useState(shadow.length);

  const addLayer = () => {
    setShiftRight(0);
    setShiftDown(0);
    setBlur(0);
    setSpread(0);

    let valueShadow = {
      color: "rgba(0,0,0,0)",
      shiftRight: 0,
      shiftDown: 0,
      blur: 0,
      spread: 0,
    };

    setShadow([...shadow, valueShadow]);
    setIndexChange(shadow.length);
  };

  useEffect(() => {
    setShadow([
      ...shadow.slice(0, indexChange),
      {
        color: "rgba(0,0,0,0)",
        shiftRight: shiftRight,
        shiftDown: shiftDown,
        blur: blur,
        spread: spread,
      },
      ...shadow.slice(indexChange + 1),
    ]);
  }, [shiftRight, shiftDown, spread, blur]);

  return (
    <div className="app">
      <h5>Shift right</h5>
      <input
        onChange={(e) => {
          setShiftRight(parseInt(e.target.value));
        }}
        value={shiftRight}
        type="number"
      />

      <h5>Shift down</h5>
      <input
        onChange={(e) => {
          setShiftDown(parseInt(e.target.value));
        }}
        value={shiftDown}
        type="number"
      />

      <h5>Spread</h5>
      <input
        onChange={(e) => {
          setSpread(parseInt(e.target.value));
        }}
        value={spread}
        type="number"
      />

      <h5>Blur</h5>
      <input
        onChange={(e) => {
          setBlur(parseInt(e.target.value));
        }}
        value={blur}
        type="number"
      />

      <h5>Add Layer</h5>
      <button onClick={addLayer}>Add</button>

      <h5>Layer: </h5>
      {shadow.length !== 0 &&
        shadow.map((item, index) => (
          <h3
            className={`${index === indexChange && "active"}`}
            onClick={() => {
              setIndexChange(index);
            }}
            key={index}
          >
            Layer {index + 1}: {item.color} {item.shiftRight}px {item.shiftDown}
            px {item.spread}px {item.blur}px
          </h3>
        ))}

      <h1>
        Result:
        <p>
          box-shadow:{" "}
          {shadow.length !== 0 &&
            shadow.map((item, index) => {
              if (index !== shadow.length - 1) {
                return `${item.color} ${item.shiftRight}px ${item.shiftDown}px ${item.spread}px ${item.blur}px, `;
              } else {
                return `${item.color} ${item.shiftRight}px ${item.shiftDown}px ${item.spread}px ${item.blur}px;`;
              }
            })}
        </p>
      </h1>
    </div>
  );
};

export default App;
